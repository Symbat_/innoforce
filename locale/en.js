export default {
  //menu
  home: "Home",
  about: "About Us",
  products: "Products",
  contacts: "Contacts",
  for_partners: "Partners",
  career: "Career",

  //meta
  home_meta: {
    title: "Innovative Products and IT Solutions - Innoforce ",
    description:
      "Innoforce is a rapidly growing IT company in Kazakhstan since 2009. We create innovative products like Avtobys and Innopay, ensuring transparency and security. Our team is led by experienced leaders in the IT industry. Learn more about us and our achievements in developing information systems.",
    keywords:
      "innovations, IT company, IT, Avtobys, Innopay, Innoforce, mobile payments, public transport payment, Kazakhstan, technologies, automated systems, bus, solutions, leaders, development, information systems, company history, start, expansion, stability, ambition, innovations",
  },
  about_meta: {
    title: "About us - Innoforce Team - Inspiration for innovation",
    description:
      'Learn about the "Innoforce" team, creators of innovative products and services that simplify people`s daily lives. Discover how they promote open communication, foster a positive atmosphere, and act as reliable entrepreneurs. Why partners recognize their professionalism and efficiency.',
    keywords:
      "buses, innoforce, innopay, IT, innovations, products, services, simplifying life, quality, strategy, values, open communication, entrepreneurship, industry trends, customer orientation, professionalism, certificates",
  },
  products_meta: {
    title: "Innoforce IT Solutions",
    description:
      "Explore the company's innovative products based on the latest technologies in IT, fintech, and mobile payments. Pay for transportation and make various payments with Avtobys. Use Innopay for mobile payments and money transfers. Discover past projects like TAIS-2, VOSTOKPLAT, EAIS, and more.",
    keywords:
      "Avtobys, bus, Innopay, innoforce, products, innovations, IT, application, ticket payment, electronic wallet, fintech, TAIS-2, VOSTOKPLAT, EAIS, mobile payments, information systems, development, Kazakhstan, Tajikistan",
  },
  contacts_meta: {
    title: "Contacts - Innoforce Group",
    description:
      "Opportunities to contact us. Our social networks. For any suggestions and questions, you can contact us by email innoforce.kz{'@'}gmail.com or at the address Republic of Kazakhstan, Almaty city, Almaly district st. Zhibek Zholy, 135",
    keywords:
      "Avtobys, Innopay, innoforce, contacts, social networks, e-mail, address, Almaty, Kazakhstan, ТОО «InnoForce Group»",
  },
  for_partners_meta: {
    title: "Partnerships with Innoforce -  Opportunities and benefits",
    description:
      "Discover collaboration opportunities with Innoforce, the creator of innovative products and services. Become a partner and explore our achievements: operating in 12 cities in Kazakhstan, 2M+ users, 1M+ downloads, 100% transparency. Partnership with Innoforce offers a solid foundation, ongoing support, and growing advantages.",
    keywords:
      "Avtobys, Innopay, innoforce, partnership, collaboration, partner-operators, achievements, cities, advantages, innovations, strategy, clients, support, profit,",
  },
  career_meta: {
    title: "Careers at Innoforce - Vacancies for better development",
    description:
      "Join the Innoforce team and work on products used by millions of people and entire cities. Explore current job openings, submit your resume via email or through the form on the page.",
    keywords:
      "IT, Avtobys, Innopay, Innoforce, application, development, vacancies, career, job, team, hr, resume, remote, innovations, products, stability, growth, experience, training",
  },

  address_text:
    "Republic of Kazakhstan, Almaty, Almaly district, Zhibek Zholy Avenue, 135 block 2, 5th floor",
  home_top_title: "We develop innovative products",
  home_top_text:
    "We are a fast-growing Kazakhstani IT company, and our journey began in 2009. Over this time, our team of experienced specialists has implemented numerous bold IT ideas and continues to develop successfully.",
  more: "Read more",
  our_products: "Our products",
  our_products_text:
    "Our products and solutions are entirely domestic and developed without government involvement. Products such as the 'Avtobys' system, the Innopay electronic money platform, and contactless payments via Bluetooth protocol have corresponding copyrights and are a service model with our own investments.",
  products_list: [
    {
      text: "public transportation fare payment",
    },
    {
      text: "electronic wallet",
    },
  ],
  power_innovation: "the power of innovations",
  our_leaders: "Our Leaders",
  read: "Read",
  more_about_us: "Learn more about us",
  leaders: [
    {
      name: "Nazerke Zhanabek",
      position: "CEO",
      text: '“Having considerable experience in the IT industry, I make informed and strategic decisions. A deep understanding of business processes in the IT field is a key element of successful management, allowing you to effectively build development strategies. With a leadership approach based on the principles of balance, tolerance and confidence in my beliefs, I strive to create a stimulating environment for self-development and creativity, where cooperation and an innovative approach to solving problems are appreciated."',
    },
    {
      name: "Zarema Aubakirova",
      position: "Chief Financial Officer",
      text: "“I have a Master of Arts in Economics degree. Having considerable experience in the field of finance, I am engaged in budgeting, control and analysis of budget execution. I control the flow of funds. As a director, I implement financial management and modeling tools, as well as do a thorough performance analysis.”",
    },
    {
      name: "Aidos Tokhtaruly",
      position: "Chief Commercial Officer",
      text: "“With extensive sales experience, I have a deep understanding of market needs and good business orientation. As part of my work, I have successfully expanded the range of sales within the company and demonstrated the ability to effectively interact with various departments. My mission is to provide customers with high-quality and simple products that contribute to increasing the company's revenue and sales volume.”",
    },
    {
      name: "Sanzhar Aubakirov",
      position: "Chief Technology Officer",
      text: "“As the technical director of Innoforce, I combine the skills of a leader, a scientist engineer and an entrepreneur. With extensive experience in computer engineering and business, I am also actively involved in scientific and educational activities. Work experience at leading universities in Kazakhstan helps to innovate in the IT business and inspire our team to new achievements.”",
    },
    {
      name: "Konstantin Starikov",
      position: "Consulting Director",
      text: "“I have extensive experience in the development of information systems architecture. As a director, I actively delegate tasks and implement functionality.”",
    },
  ],
  history_title: "Innoforce history",
  history: [
    {
      title: "Start",
      text: "Our IT company was founded in 2009. We started with a small team of experienced specialists and embarked on the development and implementation of an automated information system for the Customs Service under the Government of the Republic of Tajikistan.",
    },
    {
      title: "Expansion",
      text: "While working on our initial project, we decided to expand our horizons and took on the development of several projects related to information systems simultaneously:",
      list: [
        {
          text: "'Selective Risk Management Control' for the 'Electronic Customs' system.",
        },
        {
          text: "'Customs Automated Information System.'",
        },
        {
          text: "'Statistics, Analysis, Organization of Access to Foreign Trade Statistics Data.'",
        },
      ],
    },
    {
      title: "Stability",
      text: "Several projects were on the horizon for implementation, such as the 'Integrated Data Warehouse' and the development of components for the 'e-Minfin' information system.",
    },
    {
      title: "Aspiration",
      text: "Over the course of two years, we set ourselves the comprehensive task of refining and launching all the initiated projects.",
    },
    {
      title: "Successful implementation",
      text: "By the beginning of 2014, we received an order from the General Prosecutor's Office of the Republic of Kazakhstan to develop an information system “For official use.” The project was successfully developed within 1 year.",
    },
    {
      title: "Innovation",
      text: 'Following the successful launches of the projects, our team began to provide services for data collection, aggregation, and processing, as well as the generation of analytics and reporting. Since 2015, Innoforce has been supporting customs administration information systems."',
    },
    {
      title: "Integrated solution approaches",
      text: "During periods of work on information systems for customs administration, our team was faced with various types of tasks that required a new solution approach, which we successfully dealt with and stayed afloat.",
    },
    {
      title: "New Horizons",
      text: "After working on projects for a long time, the team decided to complete the development of projects for the customs sector and open up new horizons for themselves, starting to carefully consider the implementation of a new product.",
    },
    {
      title: "Avtobys project",
      text: "In 2019, we launched our own multifunctional product “Avtobys”, which would be happily welcomed by residents of the cities of Kazakhstan. For the past 4 years, we have been continuously improving and improving our product to make it easy and convenient for users!",
    },
  ],
  our_vacancies: "Our vacancies",
  our_vacancies_text:
    "If you want to work on products used by millions of people and entire cities, come to us.",
  career: "Career",
  career_text_1: "Do you want to work on our team?",
  career_text_2: "Send your resume to our email",
  career_text_3:
    "or write to us using the form below, and we will definitely contact you.",
  current_vacancies: "Current vacancies",
  business_analyst: "Business analyst",
  frontend_developer: "Frontend developer",
  smm_manager: "SMM manager",
  designer: "Designer",
  project_manager: "Project manager",
  remotely: "Remote",
  almaty: "Almaty city",
  astana: "Astana city",
  aktobe: "Aktobe city",
  name: "Name",
  phone_number: "Phone number",
  email: "Email",
  comments: "Comments",
  attach_resume: "Attach Resume",
  send_resume: "Write to us",
  send: "Send",
  page_not_found: "Page not found",
  our_capabilities: "Our capabilities",
  our_capabilities_text:
    "We create innovative products and services, putting a piece of",
  our_capabilities_text2:
    "our soul into them. That's why they are so beneficial to people. We understand what society needs.",
  become_partner: "Become a Partner",
  achievements: "Achievements",
  achievements_text1:
    "Our domestic products bring a significant number of users and downloads in dozens of cities in Kazakhstan.",
  achievements_text2:
    "With Innoforce, you can process a large number of transactions and taxes.",
  million: "M+",
  cities: "cities",
  users: "users",
  transparency: "transparency",
  downloads: "downloads",
  our_partners: "Our partners",
  our_partners_text:
    "For Innoforce, partnership is at the core of our work, and long-term relationships based on mutually beneficial cooperation are the key to joint success. Therefore, we value and take pride in each of our partners!",
  operator_partners: "Operator Partners",
  operator_partners_text:
    "Thanks to partners in cities, opportunities for implementing our system have opened up in cities across Kazakhstan.",
  why_choose_innoforce: "Why Choose Innoforce?",
  why_choose_innoforce_list: [
    {
      title: "Solid Foundation",
      text: "Our company sets you up for success from the start and implements fully proprietary developments.",
    },
    {
      title: "Growing Advantages",
      text: "As you grow as a partner of Innoforce, so do your opportunities. Constant improvements and innovations.",
    },
    {
      title: "Continuous Support",
      text: "Scheduled meetings with partners for joint issue resolution.",
    },
    {
      title: "Joint Efforts",
      text: "Strategic guidance on attracting new clients, implementing new features, and optimizing performance.",
    },
  ],
  cities_where_we_are: "Cities where we are present",
  our_products_text2:
    "We implement innovative projects, applying our own solutions based on the latest achievements in IT, fintech, and mobile payments.",
  avtobys: {
    title: "Avtobys system",
    text1:
      "Avtobys is a reliable tool for paying for transportation and various services, which can be used at any time.",
    text2:
      "The product has already been launched and is successfully operating in 15 cities in Kazakhstan, such as Astana, Atyrau, Aktobe, Beineu, Uralsk, Pavlodar, Semey, Ekibastuz, Aksu, Konayev, Khromtau, Zhezkazgan, Uzynagash, Ayagoz and Aksay.",
  },
  innopay: {
    title: "Innopay System",
    text1:
      "The Innopay mobile payment system was developed and launched in Kazakhstan in 2016.",
    text2:
      "This is an innovative system that allows for any payments, money transfers, and the purchase of goods and services from the mobile wallet account.",
  },
  past: "Other projects",
  past_list: [
    {
      title: "TAIS-2",
      text: 'Development and implementation of the "Customs Automated Information System" (TAIS-2) for the Committee of State Revenues of the Ministry of Finance of the Republic of Kazakhstan.',
    },
    {
      title: "UAIS (Unified Automated Information System)",
      text: 'Development and Implementation of the "Unified Automated Information System" (UAIS) for the Customs Service under the Government of the Republic of Tajikistan with the support of the Asian Development Bank.',
    },
    {
      title: "Automated Analysis and Forecasting Complex",
      text: 'Development and implementation of the "Automated Analysis and Forecasting Complex" information system for the Ministry of Economy of the Republic of Kazakhstan.',
    },
    {
      title: "For internal use",
      text: 'Development of the "For internal use" information system for the General Prosecutor`s Office of the Republic of Kazakhstan.',
    },
    {
      title: "VOSTOKPLAT",
      text: 'Development of a portal for invoicing repairs and subsequent payment for the "Housing and Utilities Management of East Kazakhstan Region" (LLP "ЖЭУ ВКО").',
    },
  ],
  about_title: "We are innoforce.kz",
  about_text:
    "We create innovative products and services, putting a piece of our soul into them. That's why they are so beneficial to people. We understand what society needs.",
  mission: "Mission",
  mission_text: "Simplify the everyday lives of people.",
  strategy: "Strategy",
  strategy_text1:
    "Develop and improve our products and services, staying ahead of the competition and remaining indispensable to society.",
  strategy_text2:
    "Expand our activities into new markets and strengthen our position on the global stage.",
  our_values: "Our Values",
  our_values_list: [
    {
      title: "Open Communications",
      text: "We have no secrets from our employees. We always openly discuss results and plans. Have a question? It will always be answered!",
    },
    {
      title: "Never Stop",
      text: "We keep an eye on all industry trends. It's not about keeping up with the times; it's about staying ahead of them.",
    },
    {
      title: "Favorable atmosphere",
      text: "The INNOFORCE team is one big family, where everyone helps and cares about each other.",
    },
    {
      title: "Customer-Centric Approach",
      text: "Our users are always confident in their choice. They choose quality and convenience. While you're still thinking, we're already taking action!",
    },
    {
      title: "Act like an entrepreneur",
      text: "Working in our team is not about strict schedules and clocking in and out.",
    },
  ],
  reviews: "What partners say about us?",
  reviews_list: [
    {
      title: "Smart Avtobys PV",
      text: "“Innoforce team has demonstrated a deep understanding of our needs by developing and implementing innovative solutions that have allowed us to significantly improve the efficiency and quality of our services.”",
    },
    {
      title: "SmartQala Oral",
      text: "“We especially appreciate Innoforce's responsible approach to fulfilling the tasks set, as well as their willingness to respond promptly to any questions that arise.”",
    },
    {
      title: "Smart Avotbys Aqtobe",
      text: "“We want to highlight a convenient mobile application and modern terminals that have significantly improved the experience of using public transport. The team has shown outstanding flexibility in creating solutions that meet modern standards and the expectations of our customers.”",
    },
  ],
  certificates: "Certificates",
  our_contacts: "Our contacts",
  address:
    "Republic of Kazakhstan, Almaty city, Almaly district, st. Zhibek Zholy, 135",
  social: "We are on social media networks",
  too: 'LLP “Innoforce solutions”',
  sent: "Sent",
  partners_text: {
    first: "CTS Astana",
    second: "Aktobe city",
    third: "Zhezkazgan city",
    fourth: "Smart Concept Semey",
    fifth: "SmartQala Atyrau",
    sixth: "SmartQala Oral",
    seventh: "Pavlodar city, Ekibastuz, Aksu",
  },
};
