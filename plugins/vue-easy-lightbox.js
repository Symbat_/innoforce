import VueEasyLightbox from "vue-easy-lightbox";

export default defineNuxtPlugin((nuxt) => {
  nuxt.vueApp.use(VueEasyLightbox);
});
