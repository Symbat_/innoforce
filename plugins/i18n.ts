import { createI18n } from "vue-i18n";
import ru from "../locale/ru";
import kz from "../locale/kz";
import en from "../locale/en";

export default defineNuxtPlugin(async (nuxt) => {
  let storage;
  if (process.client) {
    storage = localStorage.myLocale;
  }
  let locale = storage || "kz";

  const i18n = createI18n({
    legacy: false,
    globalInjection: true,
    warnHtmlMessage: false,
    locale: locale,
    fallbackWarn: false,
    missingWarn: false,

    messages: {
      ru,
      kz,
      en,
    },
  });
  nuxt.vueApp.use(i18n);
});
